<?php

namespace App\Models;

use PDO;

/**
 * Example task model
 */
class Task extends \Core\Model
{

    /**
     * Get all the tasks as an associative array
     *
     * @return array
     */
    public static function getAll($page = null, $field = null, $direction = null)
    {
        $db = static::getDB();

        $sort = '';
        if ($field != null && $direction != null) {
            $sort = " ORDER BY `$field` $direction";
        }
        if ($page > 0) {

            $perpage = 2;
            $offset = ($page - 1) * $perpage;

            $stmt = $db->query("SELECT COUNT(*) as CNT FROM tasks $sort");
            $total_rows = $stmt->fetch(PDO::FETCH_ASSOC)['CNT'];
            $total_pages = ceil($total_rows / $perpage);

            $stmt = $db->query("SELECT * FROM tasks $sort LIMIT $offset, $perpage");

        } else {
            $total_pages = 0;
            $stmt = $db->query("SELECT * FROM tasks $sort");
        }

        return [$stmt->fetchAll(PDO::FETCH_ASSOC), $total_pages];
    }

    /**
     * Find by Id
     *
     * @param integer $id
     * @return array
     */
    public static function find($id)
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM tasks WHERE id=' . $id);

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Create task
     *
     * @param string $email
     * @param string $firstname
     * @param string $lastname
     * @param string $task
     * @return array
     */
    public static function create($email, $firstname, $lastname, $task)
    {
        $db = static::getDB();
        $db->query("
            INSERT INTO `tasks`
                (`id`,`email`,`firstname`,`lastname`,`task`,`status`,`created_at`)
            VALUES
                (NULL,'$email','$firstname','$lastname','$task',NULL,CURRENT_TIMESTAMP);");
        $stmt = $db->query("SELECT LAST_INSERT_ID() AS `id`;");

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Update task
     *
     * @param integer $id
     * @param string $email
     * @param string $firstname
     * @param string $lastname
     * @param string $task
     * @param integer $status
     * @return boolean
     */
    public static function update($id, $email, $firstname, $lastname, $task, $status)
    {
        $db = static::getDB();
        $db->query("UPDATE `tasks` SET
            `email`='$email',
            `firstname`='$firstname',
            `lastname`='$lastname',
            `task` = '$task',
            `status`=$status,
            `updated_at`=CURRENT_TIMESTAMP
            WHERE id=$id;");

        return true;
    }

    /**
     * Remove task by Id
     *
     * @param integer $id
     * @return boolean
     */
    public static function remove($id)
    {
        $db = static::getDB();
        $db->query('DELETE FROM tasks WHERE id=' . $id);

        return true;
    }

}
