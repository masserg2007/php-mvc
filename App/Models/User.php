<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 */
class User extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM users');

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Find by Id
     *
     * @param integer $id
     * @return array
     */
    public static function find($id)
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM users WHERE id='.$id);

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Find by
     *
     * @param array $params
     * @return array
     */
    public static function findOneBy($params)
    {
        $sql = [];
        foreach($params as $key=>$value){
            array_push($sql, "`$key`='$value'");
        }
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM `users` WHERE '.implode(' AND ', $sql));

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Find by or
     *
     * @param array $params
     * @return array
     */
    public static function findOneByOr($params)
    {
        $sql = [];
        foreach($params as $key=>$value){
            array_push($sql, "`$key`='$value'");
        }
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM `users` WHERE '.implode(' OR ', $sql));

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Create user
     *
     * @param string $name
     * @param string $email
     * @param string $password
     * @param string $firstname
     * @param string $lastname
     * @param integer $roles
     * @return array
     */
    public static function create($name,$email,$password,$firstname,$lastname,$roles)
    {
        $db = static::getDB();
        $db->query("
            INSERT INTO `users`
                (`id`,`name`,`email`,`password`,`firstname`,`lastname`,`roles`,`created_at`)
            VALUES
                (NULL,'$name','$email','$password','$firstname','$lastname',$roles,CURRENT_TIMESTAMP);");
        $stmt = $db->query("SELECT LAST_INSERT_ID() AS `id`;");

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Update user
     *
     * @param integer $id
     * @param string $name
     * @param string $email
     * @param string $password
     * @param string $firstname
     * @param string $lastname
     * @param integer $roles
     * @return boolean
     */
    public static function update($id,$name,$email,$password,$firstname,$lastname,$roles)
    {
        $db = static::getDB();
        if($password != '') {
            $db->query("UPDATE `users` SET
            `name`='$name',
            `email`='$email',
            `password`='$password',
            `firstname`='$firstname',
            `lastname`='$lastname',
            `roles`=$roles,
            `updated_at`=CURRENT_TIMESTAMP
            WHERE id=$id;");
        } else {
            $db->query("UPDATE `users` SET
            `name`='$name',
            `email`='$email',
            `firstname`='$firstname',
            `lastname`='$lastname',
            `roles`=$roles,
            `updated_at`=CURRENT_TIMESTAMP
            WHERE id=$id;");

        }
        return true;
    }
    /**
     * Remove user by Id
     *
     * @param integer $id
     * @return boolean
     */
    public static function remove($id)
    {
        $db = static::getDB();
        $db->query('DELETE FROM users WHERE id=' . $id);

        return true;
    }

}
