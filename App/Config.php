<?php

namespace App;

/**
 * Application configuration
 */
class Config
{

    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'php_mvc';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'masserg';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = 'cc453132';

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = true;
}
