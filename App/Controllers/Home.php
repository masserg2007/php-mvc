<?php

namespace App\Controllers;

use \Core\View;
use \App\Models\Task;

/**
 * Home controller
 */
class Home extends \Core\Controller
{
    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        $page = 1;
        $field = 'created_at';
        $direction = 'DESC';
        if(isset($this->route_params['page'])) {
            $page = $this->route_params['page'];
        }
        if(isset($this->route_params['field'])) {
            $field = $this->route_params['field'];
        }
        if(isset($this->route_params['direction'])) {
            $direction = $this->route_params['direction'];
        }
        $result = Task::getAll($page,$field,$direction);
        $tasks = $result[0];
        $total_pages = $result[1];
        View::renderTemplate('Home/index.html', [
            'tasks' => $tasks,
            'total_pages' => $total_pages,
            'page' => $page,
            'field' => $field,
            'direction' => $direction
        ]);
    }

}
