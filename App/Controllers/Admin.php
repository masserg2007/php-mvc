<?php

namespace App\Controllers;

use \Core\View;

/**
 * Home controller
 */
class Admin extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        if(isset($_SESSION['user']) && $_SESSION['user']['roles'] == 1) {
            View::renderTemplate('Admin/index.html');
        } else {
            $this->redirectTo('/access-denied');
        }
    }
}
