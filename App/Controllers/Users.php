<?php

namespace App\Controllers;

use \Core\View;
use \App\Models\User;

/**
 * Users controller
 */
class Users extends \Core\Controller
{
    /**
     * Login
     *
     * @return void
     */
    public function loginAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $user = User::findOneBy(['email' => $email]);
            if (password_verify($password, $user["password"])) {
                $_SESSION['user'] = [
                    'id' => $user['id'],
                    'name' => $user['name'],
                    'email' => $user['email'],
                    'firstname' => $user['firstname'],
                    'lastname' => $user['lastname'],
                    'roles' => $user['roles']
                ];
                $this->redirectTo('/');
            } else {
                $_SESSION['message'] = ['type' => 'error', 'message' => 'Wrong Login or Password!'];
            }
        }
        View::renderTemplate('User/login.html');
    }

    /**
     * Logout
     *
     * @return void
     */
    public function logoutAction()
    {
        session_destroy();
        $this->redirectTo('/');
    }

    /**
     * Register new user
     *
     * @return void
     */
    public function registerAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $confirm_password = $_POST['confirm_password'];
            $firstname = $_POST['firstname'];
            $lastname = $_POST['lastname'];

            $user = User::findOneByOr(['name' => $name, 'email' => $email]);
            if (!$user || count($user) == 0) {
                if ($password == $confirm_password) {
                    $password = password_hash($password, PASSWORD_DEFAULT);
                    $result = User::create($name, $email, $password, $firstname, $lastname, 'NULL');
                    $_SESSION['user'] = [
                        'id' => $result['id'],
                        'name' => $name,
                        'email' => $email,
                        'firstname' => $firstname,
                        'lastname' => $lastname,
                        'roles' => NULL
                    ];
                    $this->redirectTo('/');
                } else {
                    $_SESSION['message'] = ['type' => 'error', 'message' => 'Password do not match!'];
                }
            } else {
                $_SESSION['message'] = ['type' => 'error', 'message' => 'Use name or user E-mail already exists!'];
            }
        }
        View::renderTemplate('User/register.html');
    }

    /**
     * Profile
     *
     * @return void
     */
    public function profileAction()
    {
        $id = $_SESSION['user']['id'];
        $user = User::find($id);
        View::renderTemplate('User/profile.html', ['user' => $user]);
    }

    /**
     * Show all users
     *
     * @return void
     */
    public function indexAction()
    {
        if(isset($_SESSION['user']) && $_SESSION['user']['roles'] == 1) {
            $users = User::getAll();

            View::renderTemplate('User/index.html', ['users' => $users]);
        } else {
            $this->redirectTo('/access-denied');
        }
    }

    /**
     * Create new user
     *
     * @return void
     */
    public function createAction()
    {
        if(isset($_SESSION['user']) && $_SESSION['user']['roles'] == 1) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $name = $_POST['name'];
                $email = $_POST['email'];
                $password = $_POST['password'];
                $confirm_password = $_POST['confirm_password'];
                $firstname = ($_POST['firstname'] != '') ? $_POST['firstname'] : 'NULL';
                $lastname = ($_POST['lastname']) ? $_POST['lastname'] : 'NULL';
                $roles = ($_POST['roles'] > 0) ? $_POST['roles'] : 'NULL';
                $user = User::findOneByOr(['name' => $name, 'email' => $email]);
                if (!$user || count($user) == 0) {
                    if ($password == $confirm_password) {
                        $password = password_hash($password, PASSWORD_DEFAULT);
                        $result = User::create($name, $email, $password, $firstname, $lastname, $roles);
                        $_SESSION['user'] = [
                            'id' => $result['id'],
                            'name' => $name,
                            'email' => $email,
                            'firstname' => $firstname,
                            'lastname' => $lastname,
                            'roles' => $roles
                        ];
                        $this->redirectTo('/users');
                    } else {
                        $_SESSION['message'] = ['type' => 'error', 'message' => 'Password do not match!'];
                    }
                } else {
                    $_SESSION['message'] = ['type' => 'error', 'message' => 'Use name or user E-mail already exists!'];
                }
            }
            View::renderTemplate('User/create.html');
        } else {
            $this->redirectTo('/access-denied');
        }
    }

    /**
     * Edit user by id
     *
     * @return void
     */
    public function editAction()
    {
        if(isset($_SESSION['user']) && $_SESSION['user']['roles'] == 1) {
            $id = $this->route_params['id'];
            $user = User::find($id);
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $name = $_POST['name'];
                $email = $_POST['email'];
                $password = $_POST['password'];
                $confirm_password = $_POST['confirm_password'];
                $firstname = $_POST['firstname'];
                $lastname = $_POST['lastname'];
                $roles = ($_POST['roles'] > 0) ? $_POST['roles'] : 'NULL';

                $findedUser = User::findOneByOr(['name' => $name, 'email' => $email]);
                if (!$findedUser || $findedUser['id'] == $user['id']) {
                    if ($password == '' || $password == $confirm_password) {
                        if ($password != '') {
                            $password = password_hash($password, PASSWORD_DEFAULT);
                        }
                        User::update($id, $name, $email, $password, $firstname, $lastname, $roles);

                        $this->redirectTo('/users');
                    } else {
                        $_SESSION['message'] = ['type' => 'error', 'message' => 'Password do not match!'];
                    }
                } else {
                    $_SESSION['message'] = ['type' => 'error', 'message' => 'Use name or user E-mail already exists!'];
                }
            }
            View::renderTemplate('User/edit.html', ['user' => $user]);
        } else {
            $this->redirectTo('/access-denied');
        }
    }

    /**
     * Remove user by id
     *
     * @return void
     */
    public function removeAction()
    {
        if(isset($_SESSION['user']) && $_SESSION['user']['roles'] == 1) {
            $id = $this->route_params['id'];
            User::remove($id);

            $this->redirectTo('/users');
        } else {
            $this->redirectTo('/access-denied');
        }
    }

    /**
     * Show the access denied page
     *
     * @return void
     */
    public function accessDeniedAction()
    {
        View::renderTemplate('User/access_denied.html');
    }
}
