<?php

namespace App\Controllers;

use \Core\View;
use \App\Models\Task;

/**
 * Tasks controller
 */
class Tasks extends \Core\Controller
{
    /**
     * Show all tasks
     *
     * @return void
     */
    public function indexAction()
    {
        if (isset($_SESSION['user']) && $_SESSION['user']['roles'] == 1) {
            $result = Task::getAll();
            $tasks = $result[0];

            View::renderTemplate('Task/index.html', ['tasks' => $tasks]);
        } else {
            $this->redirectTo('/access-denied');
        }
    }

    /**
     * Create new user
     *
     * @return void
     */
    public function createAction()
    {
        $user_role = 0;
        if (isset($_SESSION['user']) && $_SESSION['user']['roles'] == 1) {
            $user_role = 1;
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $email = $_POST['email'];
            $firstname = ($_POST['firstname'] != '') ? $_POST['firstname'] : 'NULL';
            $lastname = ($_POST['lastname']) ? $_POST['lastname'] : 'NULL';
            $task = $_POST['task'];

            $result = Task::create($email, $firstname, $lastname, $task);

            if ($user_role == 1) {
                $this->redirectTo('/tasks');
            } else {
                $this->redirectTo('/');
            }
        }
        if ($user_role == 1) {
            View::renderTemplate('Task/create_admin.html');
        } else {
            View::renderTemplate('Task/create_user.html');
        }
    }

    /**
     * Edit task by id
     *
     * @return void
     */
    public function editAction()
    {
        if (isset($_SESSION['user']) && $_SESSION['user']['roles'] == 1) {
            $id = $this->route_params['id'];
            $task = Task::find($id);
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $email = $_POST['email'];
                $firstname = $_POST['firstname'];
                $lastname = $_POST['lastname'];
                $task = $_POST['task'];
                $status = ($_POST['status'] > 0) ? $_POST['status'] : 'NULL';

                Task::update($id, $email, $firstname, $lastname, $task, $status);

                $this->redirectTo('/tasks');
            }
            View::renderTemplate('Task/edit.html', ['task' => $task]);
        } else {
            $this->redirectTo('/access-denied');
        }
    }

    /**
     * Remove task by id
     *
     * @return void
     */
    public function removeAction()
    {
        if (isset($_SESSION['user']) && $_SESSION['user']['roles'] == 1) {
            $id = $this->route_params['id'];
            Task::remove($id);

            $this->redirectTo('/tasks');
        } else {
            $this->redirectTo('/access-denied');
        }
    }

}
