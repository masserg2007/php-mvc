$( document ).ready(function() {

    var field = $('#sort-field option:selected').val();
    var direction = $('#direction-field option:selected').val();
    $('#sort-link').attr('href', '/page/1/field/'+field+'/direction/'+direction);

    $('#sort-field').on('change', function(){
        var field = $('#sort-field option:selected').val();
        var direction = $('#direction-field option:selected').val();
        $('#sort-link').attr('href', '/page/1/field/'+field+'/direction/'+direction);
    });
    $('#direction-field').on('change', function(){
        var field = $('#sort-field option:selected').val();
        var direction = $('#direction-field option:selected').val();
        $('#sort-link').attr('href', '/page/1/field/'+field+'/direction/'+direction);
    });
});