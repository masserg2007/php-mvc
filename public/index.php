<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

session_start([
    'cookie_lifetime' => 86400,
]);

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');


/**
 * Routing
 */
$router = new Core\Router();

// Add the routes all
$router->add('{controller}/{action}');
$router->add('{controller}/{id:\d+}/{action}');

// Add specific route
$router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('page/{page:\d+}', ['controller' => 'Home', 'action' => 'index']);
$router->add('page/{page:\d+}/field/{field:\w+}/direction/{direction:\w+}', ['controller' => 'Home', 'action' => 'index']);
$router->add('access-denied', ['controller' => 'Users', 'action' => 'accessDenied']);
$router->add('login', ['controller' => 'Users', 'action' => 'login']);
$router->add('logout', ['controller' => 'Users', 'action' => 'logout']);
$router->add('register', ['controller' => 'Users', 'action' => 'register']);
$router->add('profile', ['controller' => 'Users', 'action' => 'profile']);
$router->add('admin', ['controller' => 'Admin', 'action' => 'index']);
$router->add('users', ['controller' => 'Users', 'action' => 'index']);
$router->add('tasks', ['controller' => 'Tasks', 'action' => 'index']);

$router->dispatch($_SERVER['QUERY_STRING']);
